import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		int fact=1;
		int sum=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the value for factorial");
		int n=sc.nextInt();
		for(int i=1;i<=n;i++)
		{
			fact=fact*i;
		    sum=sum+fact;
		}
		System.out.println("Factorial= "+fact);
		System.out.println("Sum= "+sum);
	}
}
