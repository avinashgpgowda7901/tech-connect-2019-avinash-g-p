import java.util.Arrays;

public class RemovingTheElement {

	public static void main(String[] args) {
		int[] arr={30,50,40,10,20,60};
		int temp=0;
		int sum=0;
		int count=0;
		
		for(int i=0;i<arr.length;i++)
		{
			for(int j=i+1;j<arr.length;j++)
			{
				if(arr[i]>arr[j])
				{
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}
		}
		System.out.println(Arrays.toString(arr));
		for(int k=1;k<arr.length-1;k++)
		{
			sum=sum+arr[k];
			count++;
		}
		System.out.println(count);
		System.out.println("Sum="+sum);
		int avg=sum/count;
		System.out.println("Average="+avg);
	}
}
