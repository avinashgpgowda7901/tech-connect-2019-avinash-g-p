import java.util.Map;
import java.util.TreeMap;

public class Map1{

	public static void main(String[] args) {
		Map<String,Integer> map=new TreeMap<String,Integer>();
		map.put("abc",1);
		map.put("bca",2);
		map.put("mca",3);
		map.put("mba",4);
		System.out.println(map.keySet());
	}	
}
