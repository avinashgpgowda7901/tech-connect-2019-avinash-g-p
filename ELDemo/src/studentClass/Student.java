package studentClass;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import StudentDao.StudentDao;
import addressDao.Address;

/**
 * Servlet implementation class Student
 */
public class Student extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Student() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Address a=new Address();
		a.setArea("JP Nagar");
		a.setCity("Bangalore");
		a.setCountry("India");
		StudentDao st=new StudentDao();
		st.setId("101");
		st.setName("Avinash");
		st.setAge("25");
		st.setPhone_no("87926086992");
		st.setAddress(a);
		System.out.println(st.getAddress());
		HttpSession session=request.getSession();
		session.setAttribute("studentDao", st);
		RequestDispatcher rd=request.getRequestDispatcher("Display.jsp");
		rd.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
