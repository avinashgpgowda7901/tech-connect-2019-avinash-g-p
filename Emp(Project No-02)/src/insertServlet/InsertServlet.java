package insertServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class InsertServlet
 */
public class InsertServlet extends HttpServlet {
	PreparedStatement ps=null;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		response.setContentType("text/html");
		HttpSession session=request.getSession(true);
		String name=request.getParameter("name");
		String id=request.getParameter("id");
		String age=request.getParameter("age");
		int a=Integer.parseInt(age);
		String salary=request.getParameter("salary");
		String designation=request.getParameter("desgination");
		int sal=Integer.parseInt(salary);
		JDBCHelper.getConnection();
		
		session.setAttribute("ID",id);
		long doj=session.getCreationTime();
		Date date=new Date(doj);
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd:MM:yyyy");
		String startDate= formatter.format(date);
		
		long lastSeen=session.getLastAccessedTime();
		Date lDate=new Date(lastSeen);
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a");
		String lastAccessTime=sdf.format(lDate);

		  try {
			ps=JDBCHelper.con.prepareStatement("Insert into employee values(?,?,?,?,?,?,?)");
			ps.setString(1,id);
			ps.setString(2,name);
			ps.setInt(3,a);
			ps.setString(4, designation);
			ps.setInt(5, sal);
			ps.setString(6, startDate);
			ps.setString(7, lastAccessTime);
			ps.execute();
			pw.write("<h3 style='color:yellow'>Details Inserted Successfully</h3>");
            RequestDispatcher rd=request.getRequestDispatcher("/CreatePage.jsp");
            rd.include(request, response);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}
}
