function validateForm() {
	  var name = document.forms["myForm"]["name"].value;
	  var name_patt=/(?=.*[a-z][A-Z])/;
	  var ph_no = document.forms["myForm"]["ph"].value;
	  var ph_patt=/(?=.*\d)/;
	  var upi=document.forms["myForm"]["pin"].value;
	  var upi_patt=/(?=.*\d)/;
	  var acc_no=document.forms["myForm"]["acc"].value;
	  var acc_no_patt=/(?=.*\d)/;
	  var age=document.forms["myForm"]["age"].value;
	  var bal=document.forms["myForm"]["bal"].value;
	  if ((ph_no.length<10)){ 
			  document.getElementById("ph_error").innerHTML="Phone number must Contain 10 digits";
	    return false;
	  }else if(!(ph_patt.test(ph_no))){
			  document.getElementById("ph_error").innerHTML="Phone number Contain only digits. No character";
			  return false;
		  }
	  if(!(upi_patt.test(upi)))
      {
		 document.getElementById("upi_error").innerHTML="UPI Pin Contains only digits";
		 return false;
      }
	  if(acc_no.length<10)
	  {
		  document.getElementById("acc_error").innerHTML="Account number must Contain 10 digits";
		  return false;
	  }else if(!(acc_no_patt.test(acc_no))){
		  document.getElementById("acc_error").innerHTML="Account number Contain only digits. No character";
		  return false;
		  }
	   if(age<18)
	   {
		   document.getElementById("age_error").innerHTML="Age must be greater than 18 eligible to create an account";
		   return false;
	   }
	   if(name.length<=3)
	   {
		   document.getElementById("name_error").innerHTML="Name is too shorten";
		   return false;
	   }else if(!(name_patt.test(name)))
		   {
		   document.getElementById("name_error").innerHTML="Name Contain only charcters no digits";
		   return false;
		   }
	   if(bal<0)
		 {
		   document.getElementById("bal_error").innerHTML="No Negative amount is allowed";
		   return false;
		 }
	   if(bal<500)
		   {
		     document.getElementById("bal_error").innerHTML="You have to deposit minimum 500rs to open an account";
		     return false;
		   }
	  }