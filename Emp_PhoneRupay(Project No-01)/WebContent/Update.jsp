<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="Update.css">
</head>
<body>
<h4 id="h4">${sessionScope.email}</h4>
<div id="logout">
<input type="submit" value="Logout" id="btn2">
</div>
<form action="Update" method="post" autocomplete="off">
<br><br>
<div id="border">
<h1 id="h1">UPDATE</h1>
<table>
<tr><td id="label">ID:</td>
<td><input type="text" name="id" id="text" placeholder="Enter Id"></td></tr>
<tr><td id="label">Age:</td>
<td><input type="text" name="age" id="text" placeholder="Enter Age"></td></tr>
</table>
<br>
<input type="submit" value="Update" id="btn1">
<input type="reset" value="Reset" id="btn2">
</div>
</form>
<p align="center" style="color:white">Back to<a href='MainPage.html' style="color:yellow"> MainPage</a>
</body>
</html>