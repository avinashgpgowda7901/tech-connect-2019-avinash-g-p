package LoginFilter;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import JDBCHelper.JDBCHelper;

/**
 * Servlet Filter implementation class Login
 */
public class Login implements Filter {

    PreparedStatement ps=null;
    ResultSet rs=null;
    
    public Login() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req=(HttpServletRequest)request;
		PrintWriter pw=response.getWriter();
		JDBCHelper.getConnection();
		String email=request.getParameter("email");
		String password=request.getParameter("pwd");
		response.setContentType("text/html");
		try {
			ps=JDBCHelper.con.prepareStatement("Select email,password from reg where email=? and password=?");
			ps.setString(1, email);
			ps.setString(2, password);
			rs=ps.executeQuery();
			if(rs.next()==true)
			{
				HttpSession session=req.getSession();
				session.setAttribute("email", email);
				session.setAttribute("password", password);
				//String e=(String) session.getAttribute("email");
				//pw.write("<h4 style='color:yellow'>"+e+" have been Logged-in Successfully</h4>");
				RequestDispatcher rd=request.getRequestDispatcher("/MainPage.jsp");
				rd.include(request, response);			
			}
			else{
				pw.write("<h4 style='color:red'>Sorry Invalid Credential...Please Enter the Correct Input</h3>");
				RequestDispatcher rd=request.getRequestDispatcher("/Login.html");
				rd.include(request, response);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
