package LoginFilter;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class ServletDemo
 */
public class LoginServlet extends HttpServlet {
	
	PreparedStatement ps=null;
	ResultSet rs=null;
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public LoginServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		JDBCHelper.getConnection();
		String email=request.getParameter("email");
		String password=request.getParameter("pwd");
		response.setContentType("text/html");
		try {
			ps=JDBCHelper.con.prepareStatement("Select email,password from reg where email=? and password=?");
			ps.setString(1, email);
			ps.setString(2, password);
			rs=ps.executeQuery();
			if(rs.next()==true)
			{
				HttpSession session=request.getSession();
				session.setAttribute("email", email);
				session.setAttribute("password", password);
				String e=(String) session.getAttribute("email");
				pw.write(e+"<h3 style='color:yellow'>have been Logged-in Successfully</h3>");
				RequestDispatcher rd=request.getRequestDispatcher("/MainPage.html");
				rd.include(request, response);
			}
			else{
				pw.write("<h4 style='color:red'>Sorry Invalid Credential...Please Enter the Correct Input</h3>");
				RequestDispatcher rd=request.getRequestDispatcher("/Login.html");
				rd.include(request, response);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
