package accountClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBCHelper.JDBCHelper;

public class ToAccount extends HttpServlet {
	
	PreparedStatement ps=null;
	ResultSet rs=null;
	RequestDispatcher rd=null;
	String acc_no;
	String amount;
	String balance;
	String upi;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ToAccount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		response.setContentType("text/html");
		upi=request.getParameter("upi");
		try {
			ps=JDBCHelper.con.prepareStatement("Select UPI_PIN from create_Acc where account=?");
			ps.setString(1, acc_no);
			rs=ps.executeQuery();
			while(rs.next())
			{
				if(rs.getString("upi_pin").equals(upi))
				{
					ps=JDBCHelper.con.prepareStatement("Update create_acc set balance=? where account=?");
					ps.setString(1, balance);
					ps.setString(2, acc_no);
					ps.executeUpdate();
					ps=JDBCHelper.con.prepareStatement("insert into history values(?,?,?,?)");
					ps.setString(1,acc_no);
					ps.setString(2,"");
					ps.setString(3,amount);
					ps.setString(4,"To Account");
					ps.execute();
					pw.write("<h4 style='color:white'>Amount Sent Successfully</h4>");
					rd=request.getRequestDispatcher("/Account.html");
					rd.include(request, response);	
				}else{
					pw.write("<h4 style='color:Black'>InCorrect Pin</h4>");
					rd=request.getRequestDispatcher("/UPI_Pin_C.html");
					rd.include(request, response);	
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JDBCHelper.getConnection();
		PrintWriter pw=response.getWriter();
		acc_no=request.getParameter("acc");
		amount=request.getParameter("amount");
		response.setContentType("text/html");
		int bal=Integer.parseInt(amount);
		System.out.println(bal);
		try {
			ps=JDBCHelper.con.prepareStatement("Select * from create_acc where account=?");
			ps.setString(1,acc_no);
			rs=ps.executeQuery();
			if(rs.next()==true)
			{
				while(rs.next())
			{
				String balnce=rs.getString("balance");
				System.out.println(balnce);
			    int amt=Integer.parseInt(balnce);
			    System.out.println(amt);
			    bal=bal+amt;   
			}
			balance=String.valueOf(bal);
			RequestDispatcher rd=request.getRequestDispatcher("/UPI_Pin_C.html");
			rd.include(request, response);	
			}else{
				pw.write("<h4 style='color:white'>Invalid Account number</h4>");
				rd=request.getRequestDispatcher("/Account.html");
				rd.include(request, response);	
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
