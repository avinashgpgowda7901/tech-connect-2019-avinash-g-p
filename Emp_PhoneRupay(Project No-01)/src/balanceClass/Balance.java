package balanceClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class Balance
 */
public class Balance extends HttpServlet {
	PreparedStatement ps=null;
	ResultSet rs=null;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Balance() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		String acc_no=request.getParameter("acc");
		String ph_no=request.getParameter("ph");
		response.setContentType("text/html");
		JDBCHelper.getConnection();
		try {
			ps=JDBCHelper.con.prepareStatement("Select balance from create_acc where account=? or phone_no=?");
			ps.setString(1, acc_no);
			ps.setString(2, ph_no);
			rs=ps.executeQuery();
			while(rs.next())
			{
				pw.write("<h4 style='color:white' target='bal'>Balance="+rs.getInt("balance")+"</h4>");
				RequestDispatcher rd=request.getRequestDispatcher("/Balance.html");
				rd.include(request, response);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
