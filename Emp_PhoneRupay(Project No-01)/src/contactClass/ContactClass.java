package contactClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class ContactClass
 */
public class ContactClass extends HttpServlet {
	PreparedStatement ps=null;
	ResultSet rs=null;
	RequestDispatcher rd=null;
	String ph_no;
	String amount;
	static String balance;
	String upi;
	int bal;
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ContactClass() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		response.setContentType("text/html");
		upi=request.getParameter("upi");
		balance=String.valueOf(bal);

		try {
			ps=JDBCHelper.con.prepareStatement("Select UPI_PIN from create_Acc where phone_no=?");
			ps.setString(1, ph_no);
			rs=ps.executeQuery();
			while(rs.next())
			{
				if(rs.getString("upi_pin").equals(upi))
				{
					ps=JDBCHelper.con.prepareStatement("Update create_acc set balance=? where phone_no=?");
					ps.setString(1, balance);
					ps.setString(2, ph_no);
					ps.executeUpdate();
					ps=JDBCHelper.con.prepareStatement("insert into history values(?,?,?,?)");
					ps.setString(1,"");
					ps.setString(2,ph_no);
					ps.setString(3,amount);
					ps.setString(4,"To Contact");
					ps.execute();
					pw.write("<h4 style='color:white'>Amount Sent Successfully</h4>");
					rd=request.getRequestDispatcher("/Contact.html");
					rd.include(request, response);	
				}else{
					pw.write("<h4 style='color:Black'>InCorrect Pin</h4>");
					rd=request.getRequestDispatcher("/UPI_Pin_C.html");
					rd.include(request, response);	
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		JDBCHelper.getConnection();
		response.setContentType("text/html");
		ph_no=request.getParameter("ph");
		amount=request.getParameter("amount");
		bal=Integer.parseInt(amount);
		try {
			ps=JDBCHelper.con.prepareStatement("Select * from create_acc where phone_no=?");
			ps.setString(1,ph_no);
			rs=ps.executeQuery();
			if(rs.next()==true)
			{
				while(rs.next())
				{
					String balnce=rs.getString("balance");
					int amt=Integer.parseInt(balnce);
					if(amt>500)
					{
						bal=bal+amt;
					}else{
						pw.write("<h4 style='color:white'>You not have enough balance</h4>");
						rd=request.getRequestDispatcher("/Contact.html");
						rd.include(request, response);	
					}
				}
				rd=request.getRequestDispatcher("/UPI_Pin_C.html");
				rd.forward(request, response);	
			}else{
				pw.write("<h4 style='color:white'>Invalid Phone number</h4>");
				rd=request.getRequestDispatcher("/Contact.html");
				rd.include(request, response);	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
