package createAcc;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class CreateAcc
 */
public class CreateAcc extends HttpServlet {
	
	PreparedStatement ps=null;
	ResultSet rs=null;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateAcc() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int amount=5000;
		String id=request.getParameter("id");
		String name=request.getParameter("name");
		String acc=request.getParameter("acc");
		String ifsc=request.getParameter("ifsc");
		String ph_no=request.getParameter("ph");
		String upi_pin=request.getParameter("pin");
		PrintWriter pw=response.getWriter();
		JDBCHelper.getConnection();
		response.setContentType("text/html");
		try {
			ps=JDBCHelper.con.prepareStatement("Select phone_no,account from create_acc where name=? and account=?");
			ps.setString(1, name);
			ps.setString(2, acc);
			rs=ps.executeQuery();
			if(rs.next()==true)
			{
				pw.write("<h4 style='color:Yellow'>Phone_no and Account No. is already exist</h4>");
				RequestDispatcher rd=request.getRequestDispatcher("/CreateAcc.html");
				rd.include(request, response);	
			}
			else{
				ps=JDBCHelper.con.prepareStatement("insert into create_acc values(?,?,?,?,?,?,?)");
				ps.setString(1, id);
				ps.setString(2, name);
				ps.setString(3, acc);
				ps.setString(4, ifsc);
				ps.setString(5, ph_no);
				ps.setInt(6, amount);
				ps.setString(7,upi_pin);
				ps.execute();
				pw.write("<h4 style='color:yellow'>Account Created Successfully</h4>");
				RequestDispatcher rd=request.getRequestDispatcher("/CreateAcc.html");
				rd.include(request, response);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
