package deleteAccount;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class DeleteAccount
 */
public class DeleteAccount extends HttpServlet {
	PreparedStatement ps=null;
	ResultSet rs=null;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteAccount() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		response.setContentType("text/html");
		JDBCHelper.getConnection();
		String acc_no=request.getParameter("acc");
		try {
		ps=JDBCHelper.con.prepareStatement("select * from create_acc where account=?");
		ps.setString(1, acc_no);
		rs=ps.executeQuery();
		if(rs.next()==true)
		{	
		ps=JDBCHelper.con.prepareStatement("Delete from create_acc where account=?");
		ps.setString(1, acc_no);
     	ps.execute();
     	pw.write("<h4 style='color:#673BB7'>Account Deleted Successfully</h4>");
     	RequestDispatcher rd=request.getRequestDispatcher("/DeleteAcc.html");
     	rd.include(request, response);
		}else{
			pw.write("<h4 style='color:red'>Invalid Account_number</h4>");
	     	RequestDispatcher rd=request.getRequestDispatcher("/DeleteAcc.html");
	     	rd.include(request, response);
		}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
