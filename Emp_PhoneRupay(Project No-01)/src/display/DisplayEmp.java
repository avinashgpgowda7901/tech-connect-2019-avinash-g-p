package display;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class DisplayEmp
 */
public class DisplayEmp extends HttpServlet {
	
	PreparedStatement ps=null;
	ResultSet rs=null;
	ResultSetMetaData rsm=null;
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayEmp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		response.setContentType("text/html");
		HttpSession session = request.getSession(false);
		String email=(String) session.getAttribute("email");
		JDBCHelper.getConnection();
			try {
			ps=JDBCHelper.con.prepareStatement("Select *from emp");
		    rs=ps.executeQuery();
		    rsm=ps.getMetaData();
		    pw.write("<body style='background:#c0c0c0'>");
		    pw.write("<h4 align='left' style='color:yellow'>"+email+"</h4>");
		    pw.write("<center>");
		    pw.write("<h1 style='color:#0000ff'>Display Employee Details</h1>");
		    pw.println("<table border=5px, solid black width=15% height=15%>");
		    String idC=rsm.getColumnName(1);
		    String nameC=rsm.getColumnName(2);
		    String ageC=rsm.getColumnName(3);
		    pw.println("<tr align='center' style='background:#00ff00'><th>"+idC+"</th><th>"+nameC+"</th><th>"+ageC+"</th></tr>");
		    
		    while(rs.next())
		    {
		    	String id=rs.getString(1);
		    	String name=rs.getString(2);
		    	String age=rs.getString(3);
		    	pw.println("<tr align='center' style='color:black'><td style='background:white'>"+id+"</td><td style='background:white'>"+name+"</td><td style='background:white'>"+age+"</td></tr>");
		    }
		    pw.println("</table>");
	    	pw.write("<br>");
			pw.write("<p>Back to<a href='MainPage.html'> MainPage</a>");
			pw.write("</center>");
			pw.write("</body>");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	}
}
