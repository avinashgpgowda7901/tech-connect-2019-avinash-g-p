package displayClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class Display
 */
public class Display extends HttpServlet {
	
	PreparedStatement ps=null;
	ResultSet rs=null;
	ResultSetMetaData rsm=null;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Display() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		response.setContentType("text/html");
		JDBCHelper.getConnection();
			try {
				ps=JDBCHelper.con.prepareStatement("Select *from emp");
		    rs=ps.executeQuery();
		    rsm=ps.getMetaData();
		    while(rs.next())
		    {
		    	pw.write("<table>");
		    	pw.write("<tr><th>'rsm.getMetaData(1)'</th>");
		    	pw.write("<tr><th>'rsm.getMetaData(2)'</th>");
		    	pw.write("<tr><th>'rsm.getMetaData(3)'</th></tr>");
		    	pw.write("<tr><td>'rs.getString(1)'</td>");
		    	pw.write("<tr><td>'rs.getString(2)'</td>");
		    	pw.write("<tr><td>'rs.getString(3)'</td>");
		    }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
	}

}
