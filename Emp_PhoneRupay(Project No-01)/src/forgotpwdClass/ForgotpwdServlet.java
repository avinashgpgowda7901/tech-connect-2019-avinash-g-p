package forgotpwdClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class ForgotpwdServlet
 */
public class ForgotpwdServlet extends HttpServlet {
	
	PreparedStatement ps=null;
	ResultSet rs=null;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ForgotpwdServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				String email=request.getParameter("email");
				PrintWriter pw=response.getWriter();
				response.setContentType("text/html");
				JDBCHelper.getConnection();
				try {
					ps=JDBCHelper.con.prepareStatement("Select email from Reg where email=?");
					ps.setString(1,email);
					rs=ps.executeQuery();
					if(rs.next()==true)
					{
						pw.write("<h4 style='color:yellow'>This email already exist try with another email</h4>");
						RequestDispatcher rd=request.getRequestDispatcher("/Register.html");
						rd.include(request, response);	
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
	}

}
