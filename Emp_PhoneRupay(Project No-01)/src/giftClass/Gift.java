package giftClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class Gift
 */
public class Gift extends HttpServlet {
	PreparedStatement ps=null;
	ResultSet rs=null;
	RequestDispatcher rd=null;
	String balance;
	String f_ph_no;
	String amount;
	String t_ph_no;
	String message;
	String upi;
	int bal;
	int amt;
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Gift() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JDBCHelper.getConnection();
		response.setContentType("text/html");
		PrintWriter pw=response.getWriter();
		upi=request.getParameter("upi");
		try {
			ps=JDBCHelper.con.prepareStatement("Select UPI_PIN from create_Acc where phone_no=?");
			ps.setString(1, f_ph_no);
			rs=ps.executeQuery();
			while(rs.next())
			{
				if(rs.getString("upi_pin").equals(upi))
				{
					ps=JDBCHelper.con.prepareStatement("Update create_acc set balance=? where phone_no=?");
					ps.setString(1, balance);
					ps.setString(2, f_ph_no);
					ps.executeUpdate();
					bal=amt+bal;
					balance=String.valueOf(bal);
					ps=JDBCHelper.con.prepareStatement("Insert into Gift values(?,?,?,?)");
					ps.setString(1, f_ph_no);
					ps.setString(2, t_ph_no);
					ps.setString(3, amount);
					ps.setString(4, message);
					ps.execute();
					ps=JDBCHelper.con.prepareStatement("insert into history values(?,?,?,?)");
					System.out.println(ps);
					ps.setString(1,"");
					ps.setString(2,t_ph_no);
					ps.setString(3,amount);
					ps.setString(4,"Send as Gift");
					System.out.println(ps.execute());
					pw.write("<h4 style='color:white'>Gift Amount Sent Successfully</h4>");
					rd=request.getRequestDispatcher("/Gift.html");
					rd.include(request, response);	
				}else{
					pw.write("<h4 style='color:Black'>InCorrect Pin</h4>");
					rd=request.getRequestDispatcher("/UPI_Pin_A.html");
					rd.include(request, response);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JDBCHelper.getConnection();
		response.setContentType("text/html");
		PrintWriter pw=response.getWriter();
		f_ph_no=request.getParameter("f_ph");
		amount=request.getParameter("amount");
		t_ph_no=request.getParameter("t_ph");
		message=request.getParameter("msg");
		bal=Integer.parseInt(amount);
		System.out.println(bal);
		try {
			ps=JDBCHelper.con.prepareStatement("Select balance from create_acc where phone_no=?");
			ps.setString(1,f_ph_no);
			rs=ps.executeQuery();
			while(rs.next())
			{
				String balnce=rs.getString("balance");
				System.out.println(balnce);
				amt=Integer.parseInt(balnce);
				System.out.println(amt);
				bal=amt-bal; 
			}
			balance=String.valueOf(bal);
			RequestDispatcher rd=request.getRequestDispatcher("/UPI_Pin_A.html");
			rd.include(request, response);	
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
}
