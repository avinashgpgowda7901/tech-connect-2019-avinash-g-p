package historyClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class History
 */
public class History extends HttpServlet {
	PreparedStatement ps=null;
	ResultSet rs=null;
	ResultSetMetaData rsm=null;
	String accountC;
	String ph_noC;
	String amountC;
	String msgC;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public History() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		response.setContentType("text/html");
		JDBCHelper.getConnection();
			try {
			ps=JDBCHelper.con.prepareStatement("Select *from history");
		    rs=ps.executeQuery();
		    rsm=ps.getMetaData();
		    pw.write("<body style='background:#e7dff4'>");
		    pw.write("<center>");
		    pw.write("<h1 style='color:black'>Transaction Through Contact</h1>");
		    pw.println("<table width=15% height=15%>");
		    ph_noC=rsm.getColumnName(2);
		    amountC=rsm.getColumnName(3);
		    msgC=rsm.getColumnName(4);
		    pw.println("<tr align='center' style='background:#673BB7'><th style='color:white'>"+ph_noC+"</th><th style='color:white'>"+amountC+"</th><th style='color:white'>"+msgC+"</th></tr>");
		    
		    while(rs.next())
		    {
		    	String ph_no=rs.getString(2);
		    	String amount=rs.getString(3);
		    	String msg=rs.getString(4);
		    	pw.println("<tr align='center' style='color:black'><td style='background:white'>"+ph_no+"</td><td style='background:white'>"+amount+"</td><td style='background:white'>"+msg+"</td></tr>");
		    }
		    pw.println("</table>");
			pw.write("</center>");
			pw.write("</body>");
			pw.write("<body style='background:white'>");
		    pw.write("<center>");
		    pw.write("<h1 style='color:black'>Transaction Through Account</h1>");
		    pw.println("<table width=15% height=15%>");
		    accountC=rsm.getColumnName(1);
		    amountC=rsm.getColumnName(3);
		    msgC=rsm.getColumnName(4);
		    pw.println("<tr align='center' style='background:#673BB7'><th style='color:white'>"+accountC+"</th><th style='color:white'>"+amountC+"</th><th style='color:white'>"+msgC+"</th></tr>");
		    
		    while(rs.next())
		    {
		    	String acc_no=rs.getString(1);
		    	String amount=rs.getString(3);
		    	String msg=rs.getString(4);
		    	pw.println("<tr align='center' style='color:white'><td style='background:#673BB7'>"+acc_no+"</td><td style='background:#673BB7'>"+amount+"</td><td style='background:#673BB7'>"+msg+"</td></tr>");
		    }
		    pw.println("</table>");
			pw.write("</center>");
			pw.write("</body>");
			} catch (SQLException e) {
				e.printStackTrace();
			} 
	}
}
