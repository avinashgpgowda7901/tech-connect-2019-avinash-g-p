package registerClass;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JDBCHelper.JDBCHelper;

/**
 * Servlet implementation class RegisterServlet
 */
public class RegisterServlet extends HttpServlet {
	PreparedStatement ps=null;
	ResultSet rs=null;
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name=request.getParameter("uname");
		String email=request.getParameter("email");
		String pwd=request.getParameter("pwd");
		String dob=request.getParameter("dob");
		String gender=request.getParameter("gender");
		PrintWriter pw=response.getWriter();
		response.setContentType("text/html");
		JDBCHelper.getConnection();
		try {
			ps=JDBCHelper.con.prepareStatement("Select email from Reg where email=?");
			ps.setString(1,email);
			rs=ps.executeQuery();
			if(rs.next()==true)
			{
				pw.write("<h4 style='color:yellow'>This email already exist try with another email</h4>");
				RequestDispatcher rd=request.getRequestDispatcher("/Register.html");
				rd.include(request, response);	
			}else{
				ps=JDBCHelper.con.prepareStatement("insert into Reg values(?,?,?,?,?)");
				ps.setString(1, name);
				ps.setString(2, email);
				ps.setString(3, pwd);
				ps.setString(4, dob);
				ps.setString(5, gender);
				ps.execute();
				pw.write("<h4 style='color:white'>Registered Successfully</h4>");
				RequestDispatcher rd=request.getRequestDispatcher("/Login.html");
				rd.include(request, response);	
			}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
