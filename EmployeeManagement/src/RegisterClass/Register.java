package RegisterClass;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import JDBCHelper.JDBCHelper;
import employeeClass.EmployeeMain;

public class Register {

	public static String email;
	public static String name;
	public static String password;
	static PreparedStatement ps=null;
	static Scanner sc=new Scanner(System.in);
	boolean b;
	
	public void userRegistration() throws ClassNotFoundException, SQLException
	{
		do{
		System.out.println("Enter your Name");
		name=sc.next();
		Pattern p=Pattern.compile("[^A-Za-z]");
		Matcher matcher=p.matcher(name);
		boolean n=matcher.find();
		b=true;
		if(n)
		{
			System.out.println("Name contains only characters");
			b=false;
		}
		}while(!(b));
		
		do{
		System.out.println("Enter your Email");
		email=sc.next();
		Pattern p=Pattern.compile("^(.+)@(.+)$");
		Matcher m=p.matcher(email);
		boolean mail=m.matches();
		b=true;
		if(!(mail))
		{
			System.out.println("Please enter the email it in email format");
			b=false;
		}
		}while(!(b));
		do{
		System.out.println("Enter your Password");
		password=sc.next();
		Pattern p=Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})");
		Matcher m=p.matcher(password);
		boolean pwd=m.matches();
		System.out.println(pwd);
		b=true;
		if(!(pwd))
		{
			System.out.println("Password must one digit one smallercase and uppercase and one special_charcter");
			b=false;
		}
		}while(!(b));
	}
	public static void insertRegistrationDetails() throws SQLException, ClassNotFoundException, InterruptedException
	{
		try{
		Register r=new Register();
		EmployeeMain em=new EmployeeMain();
		System.out.println("Loading...");
		JDBCHelper.getConnection();
		ps=JDBCHelper.con.prepareStatement("insert into Register values(?,?,?)");
		r.userRegistration();
		ps.setString(1,Register.name);
		ps.setString(2,Register.email);
		ps.setString(3,Register.password);
		ps.execute();
		System.out.println("Registration Successfully....");
		Thread.sleep(1000);
		em.userAccess();
		}
		catch(SQLException | ClassNotFoundException | InterruptedException e)
		{
			e.printStackTrace();
		}
		finally{
		    JDBCHelper.close(ps);
		    JDBCHelper.close(JDBCHelper.con);
		    sc.close();
		}
	}
}
