package deleteClass;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import JDBCHelper.JDBCHelper;
import employeeClass.EmployeeMain;

public class DeleteEmployee {
	static PreparedStatement ps=null;
	static Scanner sc=new Scanner(System.in);
	public static void deleteEmployee() throws ClassNotFoundException, SQLException
	{
		JDBCHelper.getConnection();
		ps=JDBCHelper.con.prepareStatement("select *from employee where eid=?");
		ps.setString(1, EmployeeMain.id);
		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			System.out.println();
			System.out.println(rs.getString(1)+" "+rs.getString(2)+" "+rs.getInt(3)+" "+rs.getString(4)+" "+rs.getString(5));
		}		
	}
	public static void delete() throws ClassNotFoundException, SQLException
	{
		EmployeeMain em=new EmployeeMain();
		DeleteEmployee.deleteEmployee();
		int ch=0;
		System.out.println();
		System.out.println("Are you sure you want to Delete");
		System.out.println("Press '1' for Yes");
		System.out.println("Press '2' for No");
		System.out.println("Enter your choice");
		ch=sc.nextInt();
		switch(ch)
		{
		case 1:
		{
			ps=JDBCHelper.con.prepareStatement("Delete from employee where eid=?");
			ps.setString(1, EmployeeMain.id);
			ps.execute();
			System.out.println("The "+EmployeeMain.id+" is Deleted successfully from the employee table");
			em.menuDriven();	
		}break;
		case 2:
		{
			em.menuDriven();
		}break;
		}
	}

}
