package displayClass;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import JDBCHelper.JDBCHelper;
import employeeClass.EmployeeMain;

public class Display {

	static PreparedStatement ps=null;
	public static void displayEmployee() throws SQLException, ClassNotFoundException
	{
		EmployeeMain em=new EmployeeMain();
		ResultSet rs=null;
		ResultSetMetaData rsm=null;
		JDBCHelper.getConnection();
		ps=JDBCHelper.con.prepareStatement("Select * from employee order by eid"); 
		rs=ps.executeQuery();
		rsm=ps.getMetaData();
		while(rs.next())
		{
			System.out.println(rsm.getColumnName(1)+":"+rs.getString(1));
		    System.out.println(rsm.getColumnName(2)+":"+rs.getString(2));
		    System.out.println(rsm.getColumnLabel(3)+":"+rs.getInt(3));
		    System.out.println(rsm.getColumnLabel(4)+":"+rs.getString(4));
		    System.out.println(rsm.getColumnLabel(5)+":"+rs.getInt(5));
		    System.out.println("----------------------------------------");
		}
		em.menuDriven();
		JDBCHelper.close(rs);
	    JDBCHelper.close(ps);
	}
}
