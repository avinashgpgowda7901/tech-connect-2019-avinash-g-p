package employeeClass;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Employee {
	public static String id;
	public static String name;
	public static int age;
	static Scanner sc = new Scanner(System.in);
	static Scanner scanner = new Scanner(System.in);
	static boolean c = true;
	static int count;

	public static void createEmployee() {
		System.out.println("Enter id");
		id = sc.next();
		do {
			System.out.println("Enter Name");
			name = scanner.nextLine().toUpperCase();
			c = true;
			for (int i = 0; i < name.length(); i++) {
				if (name.charAt(i) == ' ') {
					count++;
				}
			}
			Pattern p = Pattern.compile("[^A-Za-z' ']");
			Matcher m = p.matcher(name);
			boolean specialCharacter = m.find();
			if (count > 2) {
				System.out.println("Allows only two Spaces");
				count=0;
				c=false;
			} else if (specialCharacter) {
				System.out.println("Name contains only character no special character and number");
				c = false;
			}
		} while (!(c));
		do {
			try {
				System.out.println("Enter Age");
				age = sc.nextInt();
				c = true;
				if (age < 0) {
					System.out.println("Age must be in +ve");
					c = false;
				} else if (age < 18 || age > 60) {
					System.out.println("Age must be allow greater than 18 and less than 60");
					c = false;
				}
			} catch (InputMismatchException e) {
				System.out.println("Wrong Input!..");
				c = false;
				sc.next();
			}
		} while (!(c));
	}

	public void display() {
		System.out.println("Name=" + name);
		System.out.println("Age=" + age);
	}
}