package employeeClass;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import JDBCHelper.JDBCHelper;
import RegisterClass.Register;
import deleteClass.DeleteEmployee;
import displayClass.Display;
import insertClass.Insert;
import loginClass.Login;
import updateClass.Update;

public class EmployeeMain {
	static JDBCHelper jh = new JDBCHelper();
	public static boolean manager;
	public static boolean developer;
	public static boolean tester;
	public static boolean clerk;
	public static String id;
	public static int flag;
	static Scanner sc = new Scanner(System.in);
	static boolean b;

	static{
		System.out.println();
		System.out.format("%50s","EMPLOYEE MANAGEMENT SYSTEM");
		System.out.println();
	}
	public void userAccess() throws ClassNotFoundException, SQLException, InterruptedException {
		int ch = 0;
		System.out.println("1.Register");
		System.out.println("2.Login");
		System.out.println("Enter Your choice");
		ch = sc.nextInt();
		switch (ch) {
		case 1: {
			Register.insertRegistrationDetails();
		}
			break;
		case 2: {
			Login.userLogin();
		}
			break;
		default: {
			System.out.println("Invalid Choice");
			EmployeeMain em = new EmployeeMain();
			em.userAccess();
		}
		}
	}

	public void menuDriven() throws ClassNotFoundException, SQLException {
		int ch = 0;
		EmployeeMain em = new EmployeeMain();
		do {
			System.out.println("1.CREATE");
			System.out.println("2.DISPLAY");
			System.out.println("3.UPDATE");
			System.out.println("4.DELETE");
			System.out.println("5.EXIT");
			do {
				try {
					System.out.println("Enter your choice");
					ch = sc.nextInt();
					switch (ch) {
					case 1: {
						do {
							do {
								System.out.println("1.Manager");
								System.out.println("2.Developer");
								System.out.println("3.Tester");
								System.out.println("4.Clerk");
								System.out.println("5.Exit");
								try {
									System.out.println("Enter your Choice");
									ch = sc.nextInt();
									switch (ch) {
									case 1: {
										EmployeeMain.manager = true;
										Insert.insertEmployeeDetails();
									}
										break;
									case 2: {
										EmployeeMain.developer = true;
										Insert.insertEmployeeDetails();
									}
										break;
									case 3: {
										EmployeeMain.tester = true;
										Insert.insertEmployeeDetails();
									}
										break;
									case 4: {
										EmployeeMain.clerk = true;
										Insert.insertEmployeeDetails();
									}
										break;
									case 5: {
										em.menuDriven();
									}
										break;
									default:
										System.out.println("Invalid Choice");
									}
								} catch (InputMismatchException e) {
									System.out.println("Wrong Input!..");
									b = false;
									sc.next();
								}
							} while (!(b));
						} while (ch != 5);
					}
						break;
					case 2: {
						Display.displayEmployee();
					}
						break;
					case 3: {
						do {
							try {
								System.out.println("1.Manager");
								System.out.println("2.Developer");
								System.out.println("3.Tester");
								System.out.println("4.Clerk");
								System.out.println("5.Exit");
								System.out.println("Enter your choice");
								ch = sc.nextInt();
								switch (ch) {
								case 1: {
									EmployeeMain.flag = 1;
									Update.updateEmployee();
								}
									break;
								case 2: {
									EmployeeMain.flag = 2;
									Update.updateEmployee();
								}
									break;
								case 3: {
									EmployeeMain.flag = 3;
									Update.updateEmployee();
								}
									break;
								case 4: {
									EmployeeMain.flag = 4;
									Update.updateEmployee();
								}
									break;
								case 5:
								{
									em.menuDriven();
								}break;
								}
							} catch (InputMismatchException e) {
								System.out.println("Wrong Input!..");
								b = false;
								sc.next();
							}
						} while (!(b));
					}
						break;
					case 4: {
						System.out.println("Enter the Id you have to delete");
						id = sc.next();
						DeleteEmployee.delete();
					}
					case 5: {
						System.exit(1);
						EmployeeMain emp = new EmployeeMain();
						emp.menuDriven();

					}
					}
				} catch (InputMismatchException e) {
					System.out.println("Wrong Input!..");
					b = false;
					sc.next();
				}
			} while (!(b));
		} while (ch != 4);
	}

	public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException {
		EmployeeMain em = new EmployeeMain();
		em.userAccess();
	}
}
