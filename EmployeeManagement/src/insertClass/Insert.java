package insertClass;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import JDBCHelper.JDBCHelper;
import clerkClass.Clerk;
import developerClass.Developer;
import employeeClass.Employee;
import employeeClass.EmployeeMain;
import managerClass.Manager;
import testerClass.Tester;

public class Insert {
	
	static PreparedStatement ps=null;

	public static void insertEmployeeDetails() throws ClassNotFoundException, SQLException
	{
		try{
		JDBCHelper.getConnection();
		
		if(EmployeeMain.manager==true)
		{
			Employee.createEmployee();
			ps=JDBCHelper.con.prepareStatement("insert into employee values(?,?,?,?,?)");
			ps.setString(1, Employee.id);
			ps.setString(2, Employee.name);
			ps.setInt(3, Employee.age);
			ps.setString(4, Manager.designation);
			ps.setInt(5,Manager.salary);
			ps.execute();
			System.out.println("Record Inserted Successfully!...");
			EmployeeMain.manager=false;
		}
		if(EmployeeMain.developer==true)
		{
			Employee.createEmployee();
			ps=JDBCHelper.con.prepareStatement("insert into employee values(?,?,?,?,?)");
			ps.setString(1, Employee.id);
			ps.setString(2, Employee.name);
			ps.setInt(3, Employee.age);
			ps.setString(4, Developer.designation);
			ps.setInt(5,Developer.salary);
			ps.execute();
			System.out.println("Record Inserted Successfully!...");
			EmployeeMain.developer=false;
		}
		if(EmployeeMain.tester==true)
		{
			Employee.createEmployee();
			ps=JDBCHelper.con.prepareStatement("insert into employee values(?,?,?,?,?)");
			ps.setString(1, Employee.id);
			ps.setString(2, Employee.name);
			ps.setInt(3, Employee.age);
			ps.setString(4, Tester.designation);
			ps.setInt(5,Tester.salary);
			ps.execute();
			System.out.println("Record Inserted Successfully!...");
			EmployeeMain.tester=false;
		}
		if(EmployeeMain.clerk==true)
		{
			Employee.createEmployee();
			ps=JDBCHelper.con.prepareStatement("insert into employee values(?,?,?,?,?)");
			ps.setString(1, Employee.id);
			ps.setString(2, Employee.name);
			ps.setInt(3, Employee.age);
			ps.setString(4, Clerk.designation);
			ps.setInt(5,Clerk.salary);
			ps.execute();
			System.out.println("Record Inserted Successfully!...");
			EmployeeMain.clerk=false;
		}
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally{
			JDBCHelper.close(ps);
		    JDBCHelper.close(JDBCHelper.con);
		}
	}
}
