package loginClass;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import JDBCHelper.JDBCHelper;
import employeeClass.EmployeeMain;

public class Login {

	public static String name;
	public static String password;
	static int otp;
	static boolean b;
	static PreparedStatement ps = null;
	static ResultSet rs = null;
	static Scanner sc = new Scanner(System.in);

	public void login() {
		System.out.println("Enter Name");
		name = sc.next();
		System.out.println("Enter Password");
		password = sc.next();

	}

	public static void userLogin() {
		try {
			System.out.println("Loading...");
			EmployeeMain em = new EmployeeMain();
			JDBCHelper.getConnection();
			Login l = new Login();
			l.login();
			ps = JDBCHelper.con.prepareStatement("select name,password from Register where name=? and password=?");
			ps.setString(1, Login.name);
			ps.setString(2, Login.password);
			rs = ps.executeQuery();
			if (rs.next() == true) {
				int rand = (int) (Math.random() * 10000);
				System.out.println("OTP is " +"'"+ rand+"'" + " don't share your OTP with anyone");
				do {
					try {
						System.out.println("Enter the OTP");
						otp = sc.nextInt();
						b = true;
						if (otp == rand) {
							System.out.println("Login Success...");
							Thread.sleep(1000);
							em.menuDriven();
						} else {
							System.out.println("Invalid OTP");
							b = false;
						}
					} catch (InputMismatchException e) {
						System.out.println("Wrong Input!...");
						b = false;
						sc.next();
					}
				} while (!(b));
			} else {
				System.out.println("UserName Not exist first Register and then Login");
				em.userAccess();
			}
		} catch (SQLException | ClassNotFoundException | InterruptedException e) {
			e.printStackTrace();
		} finally {
			JDBCHelper.close(rs);
			JDBCHelper.close(ps);
			JDBCHelper.close(JDBCHelper.con);
			sc.close();
		}
	}

}
