package updateClass;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import JDBCHelper.JDBCHelper;
import clerkClass.Clerk;
import developerClass.Developer;
import employeeClass.EmployeeMain;
import managerClass.Manager;
import testerClass.Tester;

public class Update {
	static PreparedStatement ps=null;
	static ResultSet rs=null;
	static Scanner sc=new Scanner(System.in);
	
	public static void updateEmployee() throws ClassNotFoundException, SQLException
	{
		try{
		JDBCHelper.getConnection();
		ps=JDBCHelper.con.prepareStatement("update Employee set salary=? where designation=?");
		ps=JDBCHelper.con.prepareStatement("Select * from employee where designation=?");
		
		if(EmployeeMain.flag==1)
		{
			ps.setString(1, Manager.designation);
			rs=ps.executeQuery();
			if(rs.next()==true)
			{
			Manager.raisSalary();
			ps.setInt(1,Manager.salary);
			ps.setString(2, Manager.designation);
			ps.execute();
			System.out.println("Updated Successfully!...");
			}else{
				System.out.println();
				System.out.println("No Manager employee in your Database to update...");
				System.out.println();
			}
		}
		if(EmployeeMain.flag==2)
		{
			ps.setString(1, Developer.designation);
			rs=ps.executeQuery();
			if(rs.next()==true)
			{
			Developer.raisSalary();
			ps.setInt(1,Developer.salary);
			ps.setString(2, Developer.designation);
			ps.execute();
		    System.out.println("Updated Successfully!...");
			}else{
				System.out.println();
				System.out.println("No Developer employee in your Database to update...");
				System.out.println();
			}

		}
		if(EmployeeMain.flag==3)
		{
			ps.setString(1, Tester.designation);
			rs=ps.executeQuery();
			if(rs.next()==true)
			{
			Tester.raisSalary();
			ps.setInt(1,Tester.salary);
			ps.setString(2,Tester.designation);
			ps.execute();
		    System.out.println("Updated Successfully!...");
			}else{
				System.out.println();
				System.out.println("No Tester employee in your Database to update...");
				System.out.println();
			}

		}
		if(EmployeeMain.flag==4)
		{
			ps.setString(1, Clerk.designation);
			rs=ps.executeQuery();
			if(rs.next()==true)
			{
			Clerk.raisSalary();
			ps.setInt(1, Clerk.salary);
			ps.setString(2, Clerk.designation);
			ps.execute();
			System.out.println("Updated Successfully!...");
			}else{
				System.out.println();
				System.out.println("No Clerk employee in your Database to update...");
				System.out.println();
			}
		}
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally{
			JDBCHelper.close(ps);
		    JDBCHelper.close(JDBCHelper.con);
		}
//		System.out.println("Enter the Id");
//		String id=sc.next();
//		ps=JDBCHelper.con.prepareStatement("select *from employee where eid=?");
//		ps.setString(1, id);
//		ResultSet rs=ps.executeQuery();
//		while(rs.next())
//		{
//			if(rs.getString(4).equals(Manager.designation))
//			{
//				Manager.raisSalary();
//				ps=JDBCHelper.con.prepareStatement("update Employee set salary=? where eid=?");
//				ps.setInt(1,Manager.salary);
//				ps.setString(2, id);
//				ps.execute();
//			}
//			if(rs.getString(4).equals(Developer.designation))
//			{
//				Developer.raisSalary();
//				ps=JDBCHelper.con.prepareStatement("update Employee set salary=? where eid=?");
//				ps.setInt(1,Developer.salary);
//				ps.setString(2, id);
//				ps.execute();
//			}
//			if(rs.getString(4).equals(Tester.designation))
//			{
//				Tester.raisSalary();
//				ps=JDBCHelper.con.prepareStatement("update Employee set salary=? where eid=?");
//				ps.setInt(1,Tester.salary);
//				ps.setString(2, id);
//				ps.execute();
//			}
//			if(rs.getString(4).equals(Clerk.designation))
//			{
//				Clerk.raisSalary();
//				ps=JDBCHelper.con.prepareStatement("update Employee set salary=? where eid=?");
//				ps.setInt(1,Clerk.salary);
//				ps.setString(2, id);
//				ps.execute();
//			}
//		}
	}

}
