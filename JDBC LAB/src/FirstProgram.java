import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FirstProgram {

	public static void main(String[] args) {

		String url = "jdbc:hsqldb:hsql://localhost/";
		String uid = "SA";
		String pwd = "";
		String driveName = "org.hsqldb.jdbcDriver";
		
		Connection con = null;
		ResultSet rs=null;
		PreparedStatement ps=null;
		
		try
		{
			Class.forName(driveName);
			con = DriverManager.getConnection(url,uid,pwd);
			System.out.println("connection established!! con = "+con);
			ps=con.prepareStatement("insert into Enemies(name,reason) values(?,?)"); 
			String name="avinash";
			String reason="abc";
			ps.setString(1, name); 
			ps.setString(2, reason); 
			ps.execute(); 
			ps=con.prepareStatement("update Enemies set name=? where reason=?");
			String name1="abhilash";
			String reason1="abc";
			ps.setString(1, name1);
			ps.setString(2, reason1);
			ps.executeUpdate();
			/*ps=con.prepareStatement("delete from Enemies where name=?");
			String name2="abhilash";
			ps.setString(1, name2);
			ps.execute();*/
			ps = con.prepareStatement("select name from Enemies"); 
			//ps.execute();
			rs=ps.executeQuery();  
			while(rs.next()) 
			{  
				name = rs.getString("name");  
				//reason = rs.getString("reason"); 
				System.out.println("Name : "+name); 
			} 
		}
		catch(ClassNotFoundException | SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(con!=null)
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

}
