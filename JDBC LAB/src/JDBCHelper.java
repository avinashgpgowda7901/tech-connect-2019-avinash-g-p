import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCHelper {
	
	public static void close(ResultSet x)
	{
		if(x!=null)
		{
			try{
				x.close();
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	public static void close(Statement x)
	{
		if(x!=null)
		{
			try{
				x.close();
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	public static void close(Connection x)
	{
		if(x!=null)
		{
			try{
				x.close();
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		}
	}
	public static Connection getConnection()
	{
		Connection con=null;
		try{
			Class.forName(Constant.driveName);
			con=DriverManager.getConnection(Constant.url,Constant.uid,Constant.pwd);
		}
		catch(ClassNotFoundException | SQLException e)
		{
			e.printStackTrace();
		}
		  return con;
	}
}

