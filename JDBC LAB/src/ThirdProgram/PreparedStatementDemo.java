package ThirdProgram;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class PreparedStatementDemo {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe","system","tiger");
		PreparedStatement ps=con.prepareStatement("insert into emp values(?,?,?)");
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter thee id");
		int id=sc.nextInt();
		System.out.println("Enter the name");
		String name=sc.next();
		System.out.println("Enter the age");
		int age=sc.nextInt();
		
		ps.setInt(1,id);
		ps.setString(2, name);
		ps.setInt(3, age);
		
		ps.execute();
		con.close();
		
	}

}
