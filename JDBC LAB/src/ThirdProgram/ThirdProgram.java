package ThirdProgram;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class ThirdProgram {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		int ch=0;
		PreparedStatement ps=null;
		ResultSet rs=null;
		Scanner sc=new Scanner(System.in);
		Scanner sc1=new Scanner(System.in);
		System.out.println("Press 1 to enter names");
		System.out.println("Press 2 to delete a name");
		System.out.println("Press 3 to display the names");
		System.out.println("Press 4 to update a name");
		System.out.println("Press 5 to exit");
		System.out.println("Enter Your Choice");
		ch=sc.nextInt();
		while(ch!=5)
		{
			switch(ch)
			{
				case 1:
				{
						System.out.println("Enter the name for Insert");
						String name=sc1.nextLine();
						ps=JDBCHelper.getConnection().prepareStatement("insert into Student(name) values(?)");
						ps.setString(1, name);
						ps.execute();
						break;

				}
				case 2:
				{
					System.out.println("Enter the name for Delete");
					String name=sc1.nextLine();
					ps=JDBCHelper.getConnection().prepareStatement("Delete from Student where name=?");
					ps.setString(1, name);
					ps.execute();
					break;

				}
				case 3:
				{
					ps=JDBCHelper.getConnection().prepareStatement("Select * from Student");
					ps.executeQuery();
					break;
				}
			}
		}
	}

}
