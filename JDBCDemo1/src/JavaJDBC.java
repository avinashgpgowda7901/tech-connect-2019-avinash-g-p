import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class JavaJDBC {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe","system","tiger");
		Statement stmt=con.createStatement();
		stmt.execute("insert into emp values(101,'avinash',25)");

	}

}
