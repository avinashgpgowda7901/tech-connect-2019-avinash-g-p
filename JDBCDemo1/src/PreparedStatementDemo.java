

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class PreparedStatementDemo {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe","system","tiger");
		PreparedStatement ps=con.prepareStatement("insert into emp values(?,?,?)");
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the id");
		int id=sc.nextInt();
		System.out.println("Enter the name");
		String name=sc.next();
		System.out.println("Enter the age");
		int age=sc.nextInt();
		
		ps.setInt(1,id);
		ps.setString(2, name);
		ps.setInt(3, age);
		
		ps.execute();
		
		ps=con.prepareStatement("update emp set age=? where name=?");
		System.out.println("Enter the ag update");
		age=sc.nextInt();
		System.out.println("Enter the name which row you have to update");
		name=sc.next();
		ps.setInt(1,age);
		ps.setString(2, name);
		
		ps.execute();
		
		ps=con.prepareStatement("Delete from emp where id=?");
		System.out.println("Enter the id which row you have to delete");
		id=sc.nextInt();
		ps.setInt(1, id);
		
		ps.execute();
		
		
		ps=con.prepareStatement("select *from emp");
		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			System.out.println("Name "+rs.getString("name")+":"+"Age "+rs.getInt("age"));
		}
		con.close();

	}

}
