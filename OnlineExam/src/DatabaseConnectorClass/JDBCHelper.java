package DatabaseConnectorClass;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import constant.Constant;

public class JDBCHelper {
	
	public static Connection con;
	
	public static Connection getConnection()
	{
		try{
			Class.forName(Constant.DriverName);
			con=DriverManager.getConnection(Constant.Url,Constant.Username,Constant.Password);
			}catch(SQLException | ClassNotFoundException e)
			{
				e.printStackTrace();
			}
		return con;
	}
	public static void close(ResultSet x)
	{
		if(x!=null)
			try {
				x.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
	
	public static void close(PreparedStatement x)
	{
		if(x!=null)
			try {
				x.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
	public static void close(Connection x)
	{
		if(x!=null)
			try {
				x.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}

}
