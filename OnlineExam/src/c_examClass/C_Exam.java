package c_examClass;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import DatabaseConnectorClass.JDBCHelper;
import onlineExamClass.Exam;
import resultClass.Result;
import userClass.UserInformation;

public class C_Exam {
	
	static PreparedStatement ps=null;
	public static ResultSet rs =null;
	static String option;
    static int count;
    static boolean b;
    static int i;
	public static boolean c;
	public static Set l=new LinkedHashSet();
	
	public static void c() throws SQLException, ClassNotFoundException, InterruptedException
	{	
		JDBCHelper.getConnection();
		Exam e=new Exam();
		Scanner sc=new Scanner(System.in);
		count = 0;
		int j = 1;
		int ch = 0;
		do {
			i = (int) (Math.random() * 11);
			l.add(i);
			ps = JDBCHelper.con.prepareStatement("Select * from C_Exam where id=?");
			ps.setInt(1, i);
			for (int k = j; k <= j; k++) {
				rs = ps.executeQuery();
				while (rs.next()) {
					System.out.println(rs.getInt(1) + " " + rs.getString(2));
					System.out.println(rs.getString(3) + " " + rs.getString(4) + " " + rs.getString(5)
							+ " " + rs.getString(6));
					do {
						System.out.println("Enter your Option");
						option = sc.next();
						Pattern p = Pattern.compile("(?=.*[A-D]).");
						Matcher matcher = p.matcher(option);
						boolean validation = matcher.matches();
						b = true;
						if (!(validation)) {
							System.out.println("Invalid Option");
							b = false;
						}
					} while (!(b));
					String cid = rs.getString(7);
					ps = JDBCHelper.con.prepareStatement(
							"select correctOption,answer from c_answer where cid in(select cid from c_exam where cid=?)");
					ps.setString(1, cid);
					rs = ps.executeQuery();
					Inner:while (rs.next()) {
						if (rs.getString("CorrectOption").equals(option)) {
							count++;
							System.out.println("Correct Answer!..");
						} else {
							System.out.println("Wrong Answer!..");
							System.out.println();
						}
					}
				}
			}
			j++;
		} while (j <= 5);
		if (count < 3) {
			UserInformation.tries--;
			System.out.println("You not reach the minimum marks still you have " + "'"
					+ UserInformation.tries + "'" + " tries take a test again");
			ps = JDBCHelper.con.prepareStatement("Update userdetails set tries=? where name=?");
			ps.setInt(1, UserInformation.tries);
			ps.setString(2, UserInformation.name);
			ps.executeUpdate();
			e.startTest();
		} else if (count >= 3) {
			c = true;
			ps = JDBCHelper.con.prepareStatement("Update userdetails set c_result=? where name=?");
			ps.setInt(1, count);
			ps.setString(2, UserInformation.name);
			ps.executeUpdate();
			Result.checkResult();
		}
		System.out.println(l);
	}

}
