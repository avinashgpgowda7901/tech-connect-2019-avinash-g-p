package constant;

public interface Constant {
	public static final String DriverName="oracle.jdbc.driver.OracleDriver";
	public static final String Url="jdbc:oracle:thin:@localHost:1521:xe";
	public static final String Username="system";
	public static final String Password="tiger";
}
