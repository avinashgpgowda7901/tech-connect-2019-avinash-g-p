package cpp_ExamClass;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import DatabaseConnectorClass.JDBCHelper;
import c_examClass.C_Exam;
import onlineExamClass.Exam;
import resultClass.Result;
import userClass.UserInformation;

public class CppExam {

	static PreparedStatement ps=null;
	static ResultSet rs =null;
	static String option;
	static int count;
	static boolean b;
	static int i;
	public static boolean cpp;

	public static void cpp() throws ClassNotFoundException, SQLException, InterruptedException
	{
		JDBCHelper.getConnection();
		Exam e=new Exam();
		Scanner sc=new Scanner(System.in);
		count = 0;
		int j = 1;
		int ch = 0;
		if (C_Exam.c == true) 
		{
			do {
				count=0;
				i = (int) (Math.random() * 11);
				ps = JDBCHelper.con.prepareStatement("Select * from Cpp_Exam where id=?");
				ps.setInt(1, i);
				for (int k = j; k <= j; k++) {
					rs = ps.executeQuery();
					while (rs.next()) {
						System.out.println(rs.getInt(1) + " " + rs.getString(2));
						System.out.println(rs.getString(3) + " " + rs.getString(4) + " "
								+ rs.getString(5) + " " + rs.getString(6));
						do {
							System.out.println("Enter your Option");
							option = sc.next();
							Pattern p = Pattern.compile("(?=.*[A-D]).");
							Matcher matcher = p.matcher(option);
							boolean validation = matcher.matches();
							b = true;
							if (!(validation)) {
								System.out.println("Invalid Option");
								b = false;
							}
						} while (!(b));
						String cppid = rs.getString(7);
						ps = JDBCHelper.con.prepareStatement("select correctOption,answer from cpp_answer where cppid in(select cppid from cpp_exam where cppid=?)");
						ps.setString(1, cppid);
						rs = ps.executeQuery();
						Inner:while (rs.next()) {
							if (rs.getString("CorrectOption").equals(option)) {
								count++;
								System.out.println("Correct Answer!..");
							} else {
								System.out.println("Wrong Answer!..");
								System.out.println();
							}
							System.out.println("Press '1' To Show Answer");
							System.out.println("Press '2' To Exit");
							ch = sc.nextInt();
							switch (ch) {
							case 1: {
								System.out.println("Answer: " + rs.getString(2));
								System.out.println();
							}
							break;
							case 2: {
								break Inner;
							}
							}
						}
					}
				}
				j++;
			} while (j <= 5);
			if (count < 3) {
				UserInformation.tries--;
				System.out.println("You not reach the minimum marks still you have " + "'"
						+ UserInformation.tries + "'" + " tries take a test again");
				ps = JDBCHelper.con.prepareStatement("Update userdetails set tries=? where name=?");
				ps.setInt(1, UserInformation.tries);
				ps.setString(2, UserInformation.name);
				ps.executeUpdate();
				e.startTest();
			} else if (count >= 3) {
				cpp = true;
				ps = JDBCHelper.con.prepareStatement("Update userdetails set cpp_result=? where name=?");
				ps.setInt(1, count);
				ps.setString(2, UserInformation.name);
				ps.executeUpdate();
				Result.checkResult();
			}
		} else {
			System.out.println("First Clear 'C' then came for C++");
			System.out.println();
			e.startTest();
		}
	}
}
