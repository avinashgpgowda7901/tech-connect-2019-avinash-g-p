package instructionsClass;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import DatabaseConnectorClass.JDBCHelper;
import onlineExamClass.Exam;
import userClass.UserInformation;

public class Instructions {
	
	
	public static void display() throws ClassNotFoundException, InterruptedException
	{
		UserInformation ui=new UserInformation();
		PreparedStatement ps=null;
		ResultSet rs=null;
		try{
		JDBCHelper.getConnection();
		ps=JDBCHelper.con.prepareStatement("Select *from Instructions");
		rs=ps.executeQuery();
		System.out.println("----------Before taking a test you have few Instructions below-----------");
		System.out.println("Please read care fully");
		while(rs.next())
		{
			System.out.println(rs.getInt(1)+" "+rs.getString(2));
		}
		System.out.println();
		ui.userdetails();
		}catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
}
