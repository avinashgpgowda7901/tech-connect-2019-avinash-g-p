package onlineExamClass;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

import DatabaseConnectorClass.JDBCHelper;
import c_examClass.C_Exam;
import cpp_ExamClass.CppExam;
import java_examClass.JavaExam;
import resultClass.Result;
import userClass.UserInformation;

public class Exam {

	Scanner sc = new Scanner(System.in);
	boolean b;
	public static int count;
	static Exam e = new Exam();
	static int count1;
	static int[] rand = new int[11];

	public void startTest() throws SQLException, ClassNotFoundException, InterruptedException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		int ch = 0;
		if (UserInformation.tries == 0) {
			System.out.println("You have no more tries to take a test Login and take a test again!...");
			ps = JDBCHelper.con.prepareStatement("Select ce.C_Questions,ca.Answer from C_Answer ca Inner join C_Exam ce on ca.cid=ce.cid where id=?");
			System.out.println();
			System.out.println("Press '1' To Show Answer");
			System.out.println("Press '2' To Exit");
			ch = sc.nextInt();
			switch (ch) {
			case 1: {
				for(int i=0;i<C_Exam.l.size();i++)
				{
					ps.setInt(1, i);
					rs=ps.executeQuery();
					while(rs.next())
					{
						System.out.println();
						System.out.println("Question: "+rs.getString("C_Questions"));
						System.out.println("Answer: " + rs.getString("Answer"));
						System.out.println();
					}
				}
			}
				break;
			case 2: {
				break;
				}
			}
			Result.checkResult();
		}
		do {
			System.out.println("1. Press '1' for C");
			System.out.println("2. Press '2' for C++");
			System.out.println("3. Press '3' for Java");
			System.out.println("4. Press '4' for Exit");
			do {
				try {
					System.out.println("Enter your choice");
					ch = sc.nextInt();
					b = true;
					switch (ch) {
					case 1: {
						C_Exam.c();
					}
						break;
					case 2: {
						CppExam.cpp();
					}
						break;
					case 3: {
						JavaExam.java();
					}
						break;
					case 4: {
						UserInformation.userAcess();
					}
						break;
					}
				} catch (InputMismatchException e) {
					System.out.println("Wrong Input!..");
					b = false;
					sc.next();
				}
			} while (!(b));
		} while (ch != 4);
	}
}
