package resultClass;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import DatabaseConnectorClass.JDBCHelper;
import userClass.UserInformation;

public class Result {

	static boolean b;

	public static void checkResult() throws SQLException, ClassNotFoundException, InterruptedException {
		JDBCHelper.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ResultSetMetaData rsm = null;
		Scanner sc = new Scanner(System.in);
		int ch = 0;
		System.out.println();
		System.out.println("1.To Check Result");
		System.out.println("2.Exit");
		do {
			try {
				System.out.println("Enter your choice");
				ch = sc.nextInt();
				b = true;
				switch (ch) {
				case 1: {
					ps = JDBCHelper.con.prepareStatement("Select * from userdetails where name=?");
					ps.setString(1, UserInformation.name);
					rs = ps.executeQuery();
					rsm = ps.getMetaData();

					while (rs.next()) {
						System.out.println();
						System.out.println(rsm.getColumnName(1) + " : " + rs.getString(1));
						System.out.println(rsm.getColumnName(3) + " : " + rs.getInt(3));
						System.out.println(rsm.getColumnName(4) + " : " + rs.getInt(4));
						System.out.println(rsm.getColumnName(5) + " : " + rs.getInt(5));
						System.out.println("-------------------------------------------");
					}
				}
					break;
				case 2: {
					UserInformation.userAcess();
				}
				}
			} catch (InputMismatchException e) {
				System.out.println("Wrong Input!..");
				b = false;
				sc.next();
			}
		} while (!(b));
	}
}
