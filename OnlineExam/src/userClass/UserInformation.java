package userClass;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import DatabaseConnectorClass.JDBCHelper;
import loginClass.Login;
import onlineExamClass.Exam;
import registerClass.Register;

public class UserInformation {

	static boolean c;
	public static String name;
	public static int tries = 3;
	static Scanner sc = new Scanner(System.in);
	
	static
	{
		System.out.println();
		System.out.format("%50s", "WELCOME TO CODE FOR RANK");
		System.out.println();
	}

	public static void userAcess() throws ClassNotFoundException, SQLException, InterruptedException {
		int ch = 0;
		System.out.println("1.Register");
		System.out.println("2.Login");
		do {
			try {
				System.out.println("Enter your Choice");
				ch = sc.nextInt();
				c = true;
				if (ch <= 2) {
					switch (ch) {
					case 1: {
						Register r = new Register();
						r.insertRegistrationDetails();
					}
						break;
					case 2: {
						Login l = new Login();
						l.userLogin();
						}
					}
				} else {
					System.out.println("Wrong Choice");
					c=false;
				}
			} catch (InputMismatchException e) {
				System.out.println("Wrong Input!..");
				c = false;
				sc.next();
			}
		} while (!(c));
	}

	public void userdetails() throws SQLException, ClassNotFoundException, InterruptedException {
		Exam e = new Exam();
		JDBCHelper.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		do {
			System.out.println("Enter your Test_Id it contains alphnumeric character");
			name = sc.next();
			c = true;
			if(name.length()<4 || name.length()>6)
			{
				System.out.println("Name should contain 4 to 6 Character");
				c=false;
			}
			Pattern p = Pattern.compile("^([a-zA-Z0-9]{4,6})$");
			Matcher m = p.matcher(name);
			boolean ud = m.matches();
			if (!(ud)) {
				System.out.println("Test_Id must contain Alphnumeric character");
				c = false;
			}
			ps = JDBCHelper.con.prepareStatement("Select * from UserDetails");
			rs = ps.executeQuery();
			if (rs.next() == true && tries == 0) {
				System.out.println("The UserId already exist and The UserId out of tries");
				c = false;
			}
		} while (!(c));
		ps = JDBCHelper.con.prepareStatement("Insert into userdetails values(?,?,?,?,?)");
		ps.setString(1, name);
		ps.setInt(2, tries);
		ps.setInt(3, Exam.count);
		ps.setInt(4, Exam.count);
		ps.setInt(5, Exam.count);
		ps.execute();
		e.startTest();
	}

}
