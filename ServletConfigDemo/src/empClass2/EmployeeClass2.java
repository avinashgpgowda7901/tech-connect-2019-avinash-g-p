package empClass2;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EmployeeClass2
 */
public class EmployeeClass2 extends HttpServlet {
	
	static String driver="";
	static String url="";
	static String name="";
	static String password="";
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeeClass2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			System.out.println(name);
			System.out.println(password);
			System.out.println(driver);
			System.out.println(url);
			Class.forName(driver);
			Connection con=DriverManager.getConnection(url,name,password);
			System.out.println(con);
			String id=request.getParameter("id");
			String name=request.getParameter("name");
			String age=request.getParameter("age");
			String salary=request.getParameter("sal");
			String designation=request.getParameter("desg");
			PreparedStatement ps = con.prepareStatement("insert into employee1 values(?,?,?,?,?)");
			ps.setString(1,id);
			ps.setString(2,name);
			ps.setString(3,age);
			ps.setString(4,salary);
			ps.setString(5,designation);
			ps.execute();
			} catch (SQLException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
        ServletContext sc=getServletContext();
        driver=sc.getInitParameter("driver1");
        url=sc.getInitParameter("url1");
        name=sc.getInitParameter("name1");
        password=sc.getInitParameter("password1");
	}
	
}
