<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="InsertClass.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
	function validate() {
		var count = 0;
		var id = document.myForm.id.value;
		var id_patt = /(?=.*\d)/;
		var name = document.myForm.name.value;
		var name_patt=/(?=.*[a-zA-Z])/;
		var age = document.myForm.age.value;
		var salary = document.myForm.salary.value;
		var designation = document.myForm.desgination.value;
		var designation_patt=/(?=.*[a-zA-Z])/;

		if (!(id_patt.test(id))) {
			document.getElementById("id_error").innerHTML = "Id Contains only digits. No character";
			return false;
		}
		if(!(name_patt.test(name)))
			{
				document.getElementById("uname").innerHTML="Name contain only characters no digits";
				return false;
			}
		if (name.length <= 3) {
			document.getElementById("uname").innerHTML = "Name can contain more than three character";
			return false;
		}
		for (var i = 0; i < name.length; i++) {
			if (name.charAt(i) == ' ') {
				count++;
			}
		}
		if (count >= 3) {
			document.getElementById("uname").innerHTML = "Name doesn't contain more then three spaces";
			return false;
		}
		count = 0;
		if (age<18 || age>60) {
			document.getElementById("age").innerHTML = "Age must be greater than 18 and lesser than 60";
			return false;
		}
		if (salary < 25000) {
			document.getElementById("salary").innerHTML = "Salary not be less than 25000";
			return false;
		}
		for (var i = 0; i < designation.length; i++) {
			if (designation.charAt(i) == ' ') {
				count++;
			}
		}
		if (count > 1) {
			document.getElementById("designation").innerHTML = "Designation doesn't contain more then one space";
			return false;
		}
		count = 0;
		if(!(designation_patt.test(designation)))
		{
			document.getElementById("designation").innerHTML="Designation contain only characters no digits";
			return false;
		}
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="HomePage.css">
</head>
<body>
	<h3 align="right" style="color: yellow">
		Welcome ${ sessionScope.username }
		</h4>
		<form name="myForm" action="Insert"
			method="post" autocomplete="off">

			<h1>Employee Form</h1>
			<div id="border">
				<table>
					<tr>
						<td id="label">ID:</td>
						<td><input type="number" name="id" placeholder="Enter ID"
							id="text" required="required">
							<div id="id_error" style="color:red"></div></td>
					</tr>
					<tr>
						<td id="label">Name:</td>
						<td><input type="text" name="name" placeholder="Enter Name"
							id="text" required="required" onblur="return validate()">
							<div id="uname" style="color: red"></div></td>
					</tr>
					<tr>
						<td id="label">Age:</td>
						<td><input type="number" name="age" placeholder="Enter Age"
							id="text" required="required" onblur="return validate()">
							<div id="age" style="color: red"></div></td>
					</tr>
					<tr>
						<td id="label">Salary:</td>
						<td><input type="number" name="salary"
							placeholder="Enter Salary" id="text" required="required" onblur="return validate()">
							<div id="salary" style="color: red"></div></td>
					</tr>
					<tr>
						<td id="label">Designation:</td>
						<td><input type="text" name="desgination"
							placeholder="Enter Designation" id="text" required="required" onblur="return validate()">
							<div id="designation" style="color: red"></div></td>
					</tr>
					<tr>
						<td id="label"><input type="submit" value="Submit" id="btn1">
							<input type="reset" value="Reset" id="btn2"></td>
					</tr>
				</table>
			</div>
		</form>
</body>
</html>